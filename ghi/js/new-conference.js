

function createLocationOption(name, id) {
    return `<option selected value="${id}">${name}</option>`
}

// function createStateOption(name, abbreviation) {
//     return `<option value="${abbreviation}">${name}</option>`
// }


window.addEventListener('DOMContentLoaded', async () => {
    
    const locationURL = "http://localhost:8000/api/locations";
    try {
        const response = await fetch(locationURL)
        if (!response.ok) {
            
        } else {
            const data = await response.json();
            const selectTag = document.querySelector('#location')
            
            for (let location of data.locations) {
                const name = location.name
                const id = location.id
                const html = createLocationOption(name, id);
                selectTag.innerHTML += html
                
            }
        }
        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();


            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            const conferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            }
        })
    } catch (error) {

    }
})
