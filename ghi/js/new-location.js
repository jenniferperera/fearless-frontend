

function createStateOption(name, abbreviation) {
    return `<option value="${abbreviation}">${name}</option>`
}


window.addEventListener('DOMContentLoaded', async () => {
    
    const stateURL = "http://localhost:8000/api/states";
    try {
        const response = await fetch(stateURL)
        if (!response.ok) {
            
        } else {
            const data = await response.json();
            const selectTag = document.querySelector('#state')
            const optionTag = document.createElement("option")
            optionTag.value 

            for (let state of data.states) {
                const name = state.name
                const abbreviation = state.abbreviation
                const html = createStateOption(name, abbreviation);
                selectTag.innerHTML += html
                
                
            }
        }
        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();


            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));

            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            }
        })
    } catch (error) {

    }
})
