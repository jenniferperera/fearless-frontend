window.addEventListener('DOMContentLoaded', async () => {
    
    function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
        return `
            <div class="card shadow my-4">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
                <p class="card-text">${description}</p>
                <div class="card-footer">${startDate} - ${endDate}</div>
            </div>
            </div>
        `;
    }
    // create function to use the bootstrap message and type of alert
    function createAlert(message, type) {
        return `
        <div class="alert alert-${type} alert-dismissible fade show" role="alert">
        ${message}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>`
    }

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);
        if (!response.ok) {
            console.log("Bad Request!")
            throw new Error(`error retrieving conferences: ${response.status} ${response.statusText}`)
        // Figure out what to do when the response is bad
        } else {
        const data = await response.json();
        // Initialize index to 0
        let i = 0;


        for (let conference of data.conferences){
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const startDate = new Date(details.conference.starts).toLocaleDateString();
                const endDate = new Date(details.conference.ends).toLocaleDateString();
                const locationName = details.conference.location.name;
                const html = createCard(name, description, pictureUrl, startDate, endDate, locationName);
                const columns = [".col", ".col:nth-child(2)", ".col:nth-child(3)"]
                const column = document.querySelector(columns[i % 3]);
                column.innerHTML += html
                i++
            } else { throw new Error(`error retrieving conferences: ${detailResponse.status} ${detailResponse.statusText}`)}
        }
    }
    } catch (e) {
        const html = createAlert("Warning!", "danger")
        const errorTag = document.querySelector('#error')
        errorTag.innerHTML = html
    }
});
