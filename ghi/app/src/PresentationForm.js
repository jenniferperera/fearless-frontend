import React, { useEffect, useState } from 'react';
// import { useNavigate } from 'react-router-dom';


function PresentationForm() {
    
    // handling name, start, end, description, etc.. change
    const [presenter_name, setPresenter] = useState('');
    const handlePresenterChange = (event) => {
        const value = event.target.value;
        setPresenter(value);
    }
    
    const [presenter_email, setEmail] = useState(undefined);
    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }
    const [company_name, setCompany] = useState('');
    const handleCompanyChange = (event) => {
        const value = event.target.value;
        setCompany(value);
    }
    const [synopsis, setSynopsis] = useState('')
    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }
    const [title, setTitle] = useState('')
    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }
    const [conference, setConference] = useState('')
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }
    const [conferences, setConferences] = useState([]);
    const fetchData = async () => {
        // we are getting a list of conferences
        const conferenceURL = 'http://localhost:8000/api/conferences/';
        const response = await fetch(conferenceURL);
        if (response.ok) {
            const data = await response.json();
            // .conferences should reflect what is in insomnia data (in the list of conferences).
            setConferences(data.conferences) 
        }
    }
    // const navigate = useNavigate()
    

    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object and get information from insomnia.
        const data = {};
        data.presenter_name = presenter_name;
        data.presenter_email = presenter_email;
        data.company_name = company_name;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;
        console.log(data);
        
        const href = data.conference
        const presentationUrl = `http://localhost:8000${href}presentations/`;
        // const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);

        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);
        
            // this is necessary for reseting the form after submitting. this is made null when we use navigate. 
            setPresenter('');
            setEmail('');
            setCompany('');
            setSynopsis('');
            setTitle('');
            setConference('');
        }
        // navigate("/"); 
    }
    


    
                                                        // "/api/conferences/1/"
    // const conferenceUrl = `http://localhost:8000/api/conferences/`;
    // const fetchConfig = {
    //     method: "post",
    //     body: JSON.stringify(data),
    //     headers: {
    //         'Content-Type': 'application/json',
    //     },
    // };
    
    // it waits htmlFor page to load and waits htmlFor function fetchData. fetchData above wont run until you call it down here. 
    useEffect(() => {
        fetchData();
        console.log("render")
    }, []);

    return(
        <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new presentation</h1>
                <form onSubmit={handleSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                    <input onChange={handlePresenterChange} value={presenter_name} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                    <label htmlFor="presenter_name">Presenter name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleEmailChange} value={presenter_email} placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                    <label htmlFor="presenter_email">Presenter email</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleCompanyChange} value={company_name} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                    <label htmlFor="company_name">Company name</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleTitleChange} value={title} placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                    <label htmlFor="title">Title</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="synopsis">Synopsis</label>
                    <textarea onChange={handleSynopsisChange} value={synopsis} className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
                </div>
                <div className="mb-3">
                <select onChange={handleConferenceChange} value={conference} required name="conference" id="conference" className="form-select">
                <option value="">Choose a Conference</option>
                    {conferences.map(conference => {
                        return (
                        <option key ={conference.href} value={conference.href}>
                            {conference.name}
                        </option>
                        );
                    })}
                </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
            </div>
        </div>
        </div>
);
}

export default PresentationForm;
