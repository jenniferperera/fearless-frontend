import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';


function ConferenceForm () {

    const navigate = useNavigate()
    // submitting the form
    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};
        data.name = name;
        data.description = description;
        data.starts = starts;
        data.ends = ends;
        data.max_attendees = maxAttendees;
        data.max_presentations = maxPresentations;
        data.location = location;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);

        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);
        
            // this is necessary for reseting the form after submitting. this is made null when we use navigate. 
            setName('');
            setStart('');
            setEnds('');
            setDescription('');
            setMaxPress('');
            setMaxAtt('');
            setLocation('');
        }
        navigate("/"); 
    }
    
    // handling name, start, end, description, etc.. change
    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    
    const [starts, setStart] = useState(undefined);
    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStart(value);
    }
    const [ends, setEnds] = useState('');
    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }
    // State is how we can store variables across React. How we hold var values across re-renders. 
    // Initial state, var that can change state
    const [description, setDescription] = useState('')
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    const [maxPresentations, setMaxPress] = useState('')
    const handleMaxPresChange = (event) => {
        const value = event.target.value;
        setMaxPress(value);
    }
    const [maxAttendees, setMaxAtt] = useState('')
    const handleMaxAttChange = (event) => {
        const value = event.target.value;
        setMaxAtt(value);
    }
    const [location, setLocation] = useState('')
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }
    
    // fetching the "location" data
    const [locations, setLocations] = useState([]);
    
    const fetchData = async () => {
        const locationURL = 'http://localhost:8000/api/locations/';
    
        const response = await fetch(locationURL);
    
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    // this acts like windows DOM content. It waits for windows to load. 
    useEffect(() => {
        fetchData();
        console.log("render")
    }, []);

// JSX content(whats being shown in the screen)
    return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new Conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleStartsChange} value={starts} placeholder="Start Date" required type="date" name="starts" id="starts" className="form-control"/>
                <label htmlFor="starts">Start Date</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleEndChange} value={ends} placeholder="End Date" required type="date" name="ends" id="ends" className="form-control"/>
                <label htmlFor="ends">End Date</label>
            </div>
            <div className= "mb-3">
                <label htmlFor="description">Description</label>
                <textarea onChange={handleDescriptionChange} value={description} className="form-control" id="description" rows="3"></textarea>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleMaxPresChange} value={maxPresentations} placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Max Presentations</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleMaxAttChange} value={maxAttendees} placeholder="MaxAttendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Max Attendees</label>
            </div>
            <div className="mb-3">
                <select onChange={handleLocationChange} value={location} required id="location" name="location" className="form-select">
                    <option value="">Choose a Location</option>
                    {locations.map(location => {
                    return (
                    <option key ={location.id} value={location.id}>
                        {location.name}
                    </option>
                    );
                    })}
                </select>
            </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
        </div>
);
}
export default ConferenceForm;
