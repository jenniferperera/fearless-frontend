// Apps.js is our ROOT COMPONENT

import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import AttendConferenceForm from './AttendConferenceForm';
import MainPage from './MainPage';
import { NavLink, Route, Routes, useNavigate, RouterProvider, BrowserRouter } from 'react-router-dom'



// make sure to pass parameter when making other functions and its saying it doesn't know what "props" is.
function App({attendees}) {
  // if (attendees === undefined) {
  //   return null;
  // }



  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="locations">
            <Route path="new" element={<LocationForm /> } />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm /> } />
          </Route>
          <Route path="attendees">
            <Route path="" element={<AttendeesList attendees={attendees}/> } />
          </Route>
          <Route path="attendees">
            <Route path="new" element={<AttendConferenceForm /> } />
          </Route>
          <Route path="presentation">
            <Route path="new" element={<PresentationForm /> } />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}


//   return (
//     <>
//     <Nav />
//       <div className="container">
//         <AttendConferenceForm/>
//         {/* <ConferenceForm/> */}
//         {/* <LocationForm /> */}
//         {/* <PresentationForm/> */}
//         {/* <AttendeesList attendees={attendees} /> */}
//       </div>
//     </>
//   );
// }

export default App;


{/* <Nav /> -- this is working bc its a custom component and it doesn't need  */}
//  "./" means in the same directory. if you want to go up, syntax is "../"
// example: ../public/favicon -- to get something in public/favicon or higher level/parent folders.

// a question mark after attendees says "are you "attendees" actually there, meaning not undefined,
// and if so, return the length of that. This is basically doing what 
//  line 3 to 5 is doing.
// <div>
//   Number of attendees: {props.attendees?.length}
// </div>
